import {useState} from "react";
import styles from "/styles/Calculator.module.scss"

export default function Calculator() {
    const [s, setS] = useState("");
    const [r, setR] = useState("0");

    function calc(val) {
        try  {
            let v = eval(val.replace("sqrt", "Math.sqrt").replace("%", "/100"));
            setR(v.toString());
        } catch (_) {
            setR("Error")
        }
    }

    function handleClick(val) {
        switch (val) {
            case false:
                setS("");
                setR("0");
                break;
            case "sqrt":
                setS(`sqrt(${s})`);
                calc(s)
                break;
            case "%":
                setS(`${s}%`);
                calc(s)
                break;
            case "=":
                calc(s)
                break;
            default:
                setS(s + val);

                break;
        }
    }

    // const q_classes = `${styles.calculator__buttons__button} ${styles.calculator__buttons__buttonWhite} ${styles.calculator__buttons__buttonCircle}`
    // console.log(q_classes)
    return <div className={styles.calculatorWrapper}>
        <div className={styles.calculator}>
            <div className={styles.calculator__board}>
                <div className={styles.calculator__board__result}>{s}</div>
                <div className={styles.calculator__board__input}>{r}</div>
            </div>
            <hr className={styles.calculator__divider}/>
            <div className={styles.calculator__buttons}>
                <div className={styles.calculator__buttons__line}>
                    <button className={styles.calculator__buttons__button} onClick={() => handleClick(false)}>C</button>
                    <button className={styles.calculator__buttons__button} onClick={() => handleClick("sqrt")}>&#8730;</button>
                    <button className={styles.calculator__buttons__button} onClick={() => handleClick("%")}>%</button>
                    <button className={styles.calculator__buttons__button} onClick={() => handleClick("/")}>/</button>
                </div>
                <div className={styles.calculator__buttons__line}>
                    <button className={styles.calculator__buttons__button} onClick={() => handleClick("7")}>7</button>
                    <button className={styles.calculator__buttons__button} onClick={() => handleClick("8")}>8</button>
                    <button className={styles.calculator__buttons__button} onClick={() => handleClick("9")}>9</button>
                    <button className={styles.calculator__buttons__button} onClick={() => handleClick("*")}>*</button>
                </div>
                <div className={styles.calculator__buttons__line}>
                    <button className={styles.calculator__buttons__button} onClick={() => handleClick("4")}>4</button>
                    <button className={styles.calculator__buttons__button} onClick={() => handleClick("5")}>5</button>
                    <button className={styles.calculator__buttons__button} onClick={() => handleClick("6")}>6</button>
                    <button className={styles.calculator__buttons__button} onClick={() => handleClick("-")}>-</button>
                </div>
                <div className={styles.calculator__buttons__line}>
                    <button className={styles.calculator__buttons__button} onClick={() => handleClick("1")}>1</button>
                    <button className={styles.calculator__buttons__button} onClick={() => handleClick("2")}>2</button>
                    <button className={styles.calculator__buttons__button} onClick={() => handleClick("3")}>3</button>
                    <button className={styles.calculator__buttons__button} onClick={() => handleClick("+")}>+</button>
                </div>
                <div className={styles.calculator__buttons__line}>
                    <button className={styles.calculator__buttons__button} onClick={() => handleClick("00")}>00</button>
                    <button className={styles.calculator__buttons__button} onClick={() => handleClick("0")}>0</button>
                    <button className={styles.calculator__buttons__button} onClick={() => handleClick(".")}>,</button>
                    <button className={`${styles.calculator__buttons__button} ${styles.calculator__buttons__buttonWhite} ${styles.calculator__buttons__buttonCircle}`} onClick={() => handleClick("=")}>=</button>
                </div>
            </div>
        </div>
    </div>
}